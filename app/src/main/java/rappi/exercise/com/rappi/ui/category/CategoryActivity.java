package rappi.exercise.com.rappi.ui.category;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import rappi.exercise.com.rappi.R;

public class CategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
    }
}
